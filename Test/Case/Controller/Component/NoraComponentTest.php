<?php
App::uses('ComponentCollection', 'Controller');
tApp::uses('NoraComponent', 'Component');

class NoraComponentTest extends CakeTestCase
{
    public function setup ( )
    {
        parent::setUp();

    }

    public function testNora ( )
    {
        $this->Nora = new NoraComponent(
            new ComponentCollection()
        );


        $this->Nora->setHelper('hi', function ( ) {
            return 'hi';
        });


        $this->assertEquals(
            'hi',
            $this->Nora->hi()
        );
    }
}
