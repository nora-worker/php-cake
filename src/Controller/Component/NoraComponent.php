<?php
App::uses('Component', 'Controller');

class NoraComponent extends Component {

    /**
     * 初期化
     */
    public function __construct(ComponentCollection $collection, $settings = array())
    {
        parent::__construct($collection, $settings);

        Nora::module('cake')->configure([
            'component' => $this
        ]);
    }

    public function initialize(Controller $controller)
    {
    }

    public function __call($name, $params)
    {
        return call_user_func_array([Nora::module('cake'), $name], $params);
    }
}
