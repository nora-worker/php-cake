<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Cake;

use Nora\Core\Module\Module;

/**
 * Cakeモジュール
 */
class Facade extends Module
{
    protected function initModuleImpl( )
    {
        $this->rootScope()->setComponent('secure', $this);
    }


    protected function afterConfigure( )
    {
        $this->rootScope()->observe(function ($e) {
            $isLog = $e->match(function($tag) {
                return 0 === strpos($tag, 'log.');
            }, $hit);

            if ($isLog)
            {
                $this->config()->get('component')->log($e->msg, substr($hit, 4));
            }
        });
    }

    protected function bootConfig($settings = [])
    {
        $config = parent::bootConfig([
            'component' => false
        ]);

        return $config;
    }
}
